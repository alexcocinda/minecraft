# Gatro Minecraft Server

**Please follow the instruction closely and in the presented order.**

### Zsh

1. Install Zsh.

```bash
sudo apt update && sudo apt install -y zsh
```

2. Install Oh My Zsh.

```bash
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

### Mcrcon

1. Clone the repo.

```bash
git clone https://github.com/Tiiffi/mcrcon.git
```

2. Enter the repo.

```bash
cd mcrcon
```

3. Build and install.

```bash
make && sudo make install
```

### Install

1. Find the latest [Paper](https://papermc.io/downloads) binary version and copy the download link.

2. Export the rcon password.

```bash
export MCRCON_PASSW='<rcon_password>'
```

2. Install the server configuration.

```bash
./install.sh
Usage: install.sh [:hg:b:l:r:p:]

    -h      print this
    -g      gatro home directory
            default: /opt/gatro/minecraft
    -b      backups directory
            default: /var/opt/gatro/backups/minecraft/
    -l      logs directory
            default: /var/log/gatro
    -r      rcon password
    -p      PaperMC binary download url
```

3. Add plugins to the plugin directory.

### Management

The `mc` command covers every possible administrative operation.

```bash
-> % mc
Usage: mc <action>

    rcon       connect to rcon
    start      start server
    stop       stop server
    restart    restart server
    status     server status
    log        tail server log
    backup     backup server to /var/opt/gatro/backups/minecraft

```
