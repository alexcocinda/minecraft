#!/bin/bash

gatro_mc_home=/opt/gatro/minecraft
gatro_mc_backups=/var/opt/gatro/backups/minecraft/
gatro_mc_logs=/var/log/gatro

if [ ! -f ~/.zshrc ]; then
  echo "Install Zsh first!"
  exit
fi

optstring=":hg:b:l:r:p:"

function usage {
  printf "Usage: $(basename $0) [${optstring}]\n\n"
  printf "    -h      print this\n"
  printf "    -g      gatro home directory\n"
  printf "            default: ${gatro_mc_home}\n"
  printf "    -b      backups directory\n"
  printf "            default: ${gatro_mc_backups}\n"
  printf "    -l      logs directory\n"
  printf "            default: ${gatro_mc_logs}\n"
  printf "    -r      rcon password\n"
  printf "    -p      PaperMC binary download url\n"
}

if [[ $# -eq 0 ]]; then
  usage
  exit 0
fi

while getopts ${optstring} arg; do
  case ${arg} in
    h)
      usage
      exit 0
      ;;
    g)
      gatro_mc_home=${OPTARG}
      ;;
    b)
      gatro_mc_backups=${OPTARG}
      ;;
    l)
      gatro_mc_logs=${OPTARG}
      ;;
    r)
      rcon_password=${OPTARG}
      ;;
    p)
      paper_download_url=${OPTARG}
      ;;
    :)
      echo "$0: Must supply an argument to -${OPTARG}." >&2
      exit 1
      ;;
    ?)
      echo "Invalid option: ${OPTARG}."
      exit 2
      ;;
  esac
done

if [ -z ${rcon_password} ]; then
  echo "$(basename $0): Setting the rcon password is mandatory."
  exit 1
fi

if [ -z ${paper_download_url} ]; then
  echo "$(basename $0): Setting the PaperMC binary download url is mandatory."
  exit 1
fi

# install minecraft dependencies
sudo apt update && sudo apt install -y default-jre

# create the gatro user
sudo adduser \
   --system \
   --shell /bin/bash \
   --group \
   --disabled-password \
   --home /home/gatro \
   gatro

# create directory structure
sudo mkdir -p ${gatro_mc_home}
sudo chown -R gatro:gatro ${gatro_mc_home}
sudo mkdir -p /var/opt/gatro/backup/minecraft
sudo chown -R gatro:gatro /var/opt/gatro/backup/minecraft

# download paper binary
wget -P ${gatro_mc_home} ${paper_download_url}
sudo mv ${gatro_mc_home}/paper-*.jar ${gatro_mc_home}/paper.jar

# copy run script
sudo cp tools/run.sh ${gatro_mc_home}

#try saving time by agreeing to eula beforehand
echo "eula=true" | sudo tee -a ${gatro_mc_home}/eula.txt

# copy commands
sudo cp tools/mc /usr/local/bin
sudo chmod +x /usr/local/bin/mc
cat <<EOF >> ~/.zshenv
export GATRO_MC_HOME=${gatro_mc_home}
export GATRO_MC_BACKUPS=${gatro_mc_backups}
export GATRO_MC_LOGS=${gatro_mc_logs}
export MCRCON_PASS=${rcon_password}
EOF

# copy the config files
sudo cp rsyslog/minecraft.conf /etc/rsyslog.d/
sudo cp logrotate/minecraft.conf /etc/logrotate.d/
# create the log file
sudo mkdir -p ${gatro_mc_logs}
sudo touch ${gatro_mc_logs}/minecraft.log
sudo chmod o+r ${gatro_mc_logs}/minecraft.log
# set ownership
sudo chown root:adm ${gatro_mc_logs}/minecraft.log
# apply changes
sudo systemctl restart rsyslog

# set up systemd
sudo cp systemd/minecraft.service /etc/systemd/system/
sudo systemctl enable --now minecraft
